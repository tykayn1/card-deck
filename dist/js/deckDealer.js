
/*
core of the action in a game,
the dealer runs the turns
@returns {undefined}
 */
var DeckDealer;

DeckDealer = (function() {
  DeckDealer.prototype.jeux = [];

  DeckDealer.prototype.players = [];

  DeckDealer.prototype.deck = {};

  DeckDealer.prototype.playerToStart = 0;

  DeckDealer.prototype.playerActive = {};

  DeckDealer.prototype.jQActions = function(TheDealer) {
    var jeu, listeJeux, _i, _len, _ref;
    if (TheDealer == null) {
      TheDealer = {};
    }
    TheDealer = this;
    if (TheDealer.deck.length) {
      console.log('------- jqactions avec TheDealer');
    }
    if (!$) {
      console.error('ce jeu nécessite jquery pour fonctionner, vérifiez qu\'il est chargé');
    }
    $("body").on("click", "#input-choice .card", function(e) {
      var card, cardClicked, cardId, hand;
      cardClicked = $(this);
      cardId = cardClicked.attr("data-id");
      hand = TheDealer.activeGuy.cards;
      card = TheDealer.idToCard(cardId, hand);
      TheDealer.putCardToTable(card);
      return cardClicked.fadeOut();
    });
    listeJeux = '<ul>';
    _ref = dealer.jeux;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      jeu = _ref[_i];
      listeJeux += '<li>' + jeu.name + '</li>';
    }
    listeJeux += '</ul>';
    return $('#listeJeux').html(listeJeux);
  };

  function DeckDealer(config) {
    var dealer;
    if (config == null) {
      config = {};
    }
    console.log('new game dealer');
    if (config.players) {
      this.setPlayers(config.players);
    }
    if (config.game) {
      dealer = this;
      this.setGame(config.game);
    }
    console.log('card dealer built with ' + this.deck.length + ' cards');
  }

  DeckDealer.prototype.config = {
    autoplay: 1,
    activeGuy: {},
    graveyard: [],
    table: [],
    otherPlayer: {}
  };

  DeckDealer.prototype.table = [];

  DeckDealer.prototype.graveyard = [];

  DeckDealer.prototype.maxTableTurns = 20;

  DeckDealer.prototype.maxTurns = 200;

  DeckDealer.prototype.turn = 0;

  DeckDealer.prototype.game = {};

  DeckDealer.prototype.setPlayers = function(players) {
    this.players = players;
    return this.log('les ' + players.length + ' joueurs prennent place');
  };

  DeckDealer.prototype.setGame = function(newGame) {
    console.log('set game ');
    if (!newGame) {
      console.error('nouveau jeu invalide: ');
      console.error(newGame);
      return false;
    }
    this.game = newGame;
    this.deck = newGame.cards;
    console.log('newGame.cards ', newGame.cards.length);
    this.activeGuy = this.players[this.playerToStart];
    $('#rulesInfo').html(this.game.briefing());
    return $('.GameName').html(this.game.name);
  };

  DeckDealer.prototype.deck2html = function(cards) {
    return this.game.cards2html(cards);
  };

  DeckDealer.prototype.askInput = function(message) {
    var activeName, cards, choice;
    if (message == null) {
      message = '<br/>à vous de jouer ' + this.activeGuy.name;
    }
    this.activeGuy = this.players[this.playerActive];
    activeName = this.activeGuy.name;
    cards = [];
    if (this.activeGuy) {
      cards = this.activeGuy.cards;
      choice = '';
      choice = this.deck2html(cards);
      $('#input-choice').html(choice);
    }
    return this.setState('<h2>' + this.turn + '</h2> statoi de jouer, ' + activeName + message);
  };

  DeckDealer.prototype.setState = function(text) {
    return $('#state').html(text);
  };

  DeckDealer.prototype.addGame = function(game) {
    var g, _i, _len, _ref;
    _ref = this.jeux;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      g = _ref[_i];
      if (g.name === game.name) {
        return false;
      }
    }
    return this.jeux.push(game);
  };

  DeckDealer.prototype.shuffle = function() {
    var i, j, o, x;
    o = this.deck;
    if (!o) {
      return o;
    }
    j = void 0;
    x = void 0;
    i = o.length;
    while (i) {
      j = Math.floor(Math.random() * i);
      x = o[--i];
      o[i] = o[j];
      o[j] = x;
    }
    this.deck = o;
    return this.deck;
  };

  DeckDealer.prototype.distribute = function(player, int) {
    var i, oneCard;
    i = 0;
    if (int === 0) {
      return;
    }
    if (this.deck.length === 0) {
      return 0;
    }
    while (i < int) {
      oneCard = this.deck.pop();
      oneCard.ownerId = i;
      player.cards.push(oneCard);
      i++;
    }
    this.hasDistributed = 1;
    return i;
  };


  /*
    distribute an equal number of cards to all the players
   */

  DeckDealer.prototype.distributeAll = function(players, int) {
    var i, j, oneCard, p, _i, _len, _ref;
    console.log('le deck avait ' + this.deck.length + ' cartes');
    if (!this.deck) {
      console.error('pas de deck chez le dealer');
      return false;
    }
    i = 0;
    _ref = this.players;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      p = _ref[_i];
      if (typeof p !== undefined) {
        this.log('distribution de ' + int + ' cartes à ' + p.name);
        j = 0;
        while (j < int) {
          oneCard = this.deck.pop();
          oneCard.ownerId = i;
          p.cards.push(oneCard);
          j++;
        }
        console.log('    il a maintenant ' + p.cards.length + ' cartes');
      }
      i++;
    }
    this.hasDistributed = 1;
    this.upPlayers();
    return true;
  };

  DeckDealer.prototype.upPlayers = function() {
    var p, _i, _len, _ref, _results;
    _ref = this.players;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      p = _ref[_i];
      _results.push($('#player-' + p.id).html('<strong>' + p.name + '</strong> ' + p.cards.length + ' cartes'));
    }
    return _results;
  };

  DeckDealer.prototype.emptyTable = function() {
    var card, _i, _len, _ref;
    _ref = this.table;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      card = _ref[_i];
      this.discard(card);
    }
    return this.table = [];
  };

  DeckDealer.prototype.discard = function(card) {
    return this.game.graveyard.push(card);
  };

  DeckDealer.prototype.nextTurn = function() {
    var fightResult;
    this.turn++;
    if (this.table.length === 2) {
      fightResult = this.tableFight();
      if (fightResult === "equal") {
        this.log('égalité!');
      } else {
        this.players[fightResult].score++;
        this.log(this.players[fightResult].name + ' a gagné le match!');
      }
      this.emptyTable();
    }
    return this.isItFinished();
  };

  DeckDealer.prototype.isItFinished = function() {
    if (this.maxTurns < this.turn) {
      return this.gameOver();
    }
    if (this.activeGuy && this.activeGuy.cards.length === 0) {
      return this.winning();
    } else {
      this.setActivePlayer();
      this.refreshView();
      if (this.config.autoplay) {
        if (this.activeGuy.type === "NPC") {
          return setTimeout(this.autoplay(), 500);
        } else {

        }
      } else {
        return this.askInput();
      }
    }
  };

  DeckDealer.prototype.gameOver = function() {
    this.log(' maximum turns reached, game over');
    $("body").off("click", "#input-choice .card");
    return $("#input-choice button").disable();
  };

  DeckDealer.prototype.autoplay = function() {
    var card;
    card = this.activeGuy.cards.pop(0);
    this.refreshView();
    return this.putCardToTable(card);
  };

  DeckDealer.prototype.winning = function() {
    var txt;
    $("#input-choice, #table").fadeOut();
    txt = 'le joueur ' + this.activeGuy.name + ' est vainqueur!';
    this.activeGuy.won = 1;
    this.log(txt);
    return $("#state").html(txt);
  };

  DeckDealer.prototype.oneTurn = function() {
    this.refreshView();
    return this.askInput();
  };

  DeckDealer.prototype.idToCard = function(needle, haystack) {
    var c, i, _i, _len;
    needle = parseInt(needle);
    i = 0;
    for (_i = 0, _len = haystack.length; _i < _len; _i++) {
      c = haystack[_i];
      if (parseInt(c.id) === needle) {
        return c;
      }
    }
    console.log('card ' + needle + ' NOT found');
    return i++;
  };

  DeckDealer.prototype.idToHandId = function(needle, haystack) {
    var c, i, _i, _len;
    needle = parseInt(needle);
    i = 0;
    for (_i = 0, _len = haystack.length; _i < _len; _i++) {
      c = haystack[_i];
      if (parseInt(c.id) === needle) {
        return i;
      }
      i++;
    }
  };

  DeckDealer.prototype.getGame = function() {
    return this.game;
  };

  DeckDealer.prototype.play = function() {
    var i, log;
    this.maxTurns = this.maxTableTurns * players.length;
    this.activeGuy = this.players[this.playerToStart];
    this.playerActive = this.playerToStart;
    log = "";
    i = 1;
    return this.oneTurn();
  };

  DeckDealer.prototype.table = [];

  DeckDealer.prototype.killAllHands = function() {
    var p, _i, _len, _ref, _results;
    _ref = this.players;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      p = _ref[_i];
      _results.push(this.killPlayerHand(p));
    }
    return _results;
  };

  DeckDealer.prototype.killPlayerHand = function(player) {
    var droppedCards;
    droppedCards = player.cards;
    player.cards = [];
    return this.graveyard.push(droppedCards);
  };

  DeckDealer.prototype.putCardToTable = function(card) {
    var res;
    if (this.activeGuy) {
      card.ownerId = this.activeGuy.id;
    }
    res = this.idToHandId(card.id, this.activeGuy.cards);
    this.activeGuy.cards.splice(res, 1);
    this.table.push(card);
    if (this.activeGuy.type === "true-player") {
      console.log('   le joueur a maintenant ' + this.activeGuy.cards.length + ' cartes');
    }
    this.log('le joueur ' + this.activeGuy.name + ' pose la carte ' + card.name);
    return this.nextTurn();
  };

  DeckDealer.prototype.tableFightGo = function() {
    var SecondId, SecondPoints, firstId, firstPoints;
    firstPoints = this.table[0].points;
    firstId = this.table[0].ownerId;
    SecondPoints = this.table[1].points;
    SecondId = this.table[0].ownerId;
    if (firstPoints === SecondPoints) {
      return "equal";
    } else {
      if (firstPoints > SecondPoints) {
        this.loserAction(SecondId);
        return firstId;
      } else {
        this.loserAction(firstId);
        return SecondId;
      }
    }
  };

  DeckDealer.prototype.tableFight = function() {
    return this.tableFightGo();
  };

  DeckDealer.prototype.loserAction = function(idLoser) {
    var result, theGuy;
    theGuy = this.players[idLoser];
    result = this.distribute(theGuy, 1);
    if (result === 1) {

    } else {
      if (result === 0) {
        if (this.players[idLoser].cards.length === 0) {
          return this.winning();
        }
      }
    }
  };

  DeckDealer.prototype.setActivePlayer = function() {
    this.playerActive++;
    if (this.playerActive >= players.length) {
      this.playerActive = 0;
    }
    return this.activeGuy = this.players[this.playerActive];
  };

  DeckDealer.prototype.log = function(text) {
    if (this.turn !== this.lastTurn) {
      text = " <h2>tour " + this.turn + "</h2> " + text;
    }
    text = "<div class=\"bs-callin bs-callin-info\"><p>" + text + "</p></div>";
    $("#log").append(text);
    return this.lastTurn = this.turn;
  };

  DeckDealer.prototype.refreshView = function() {
    var cards, choice, players, status, tempCount, _results;
    players = this.players;
    $("#table").html(this.deck2html(this.table));
    $("#graveyardLength").html(this.game.graveyard.length);
    if (this.game) {
      status = this.game.health();
      $("#state").html(status);
    }
    $("#graveyard").html(this.deck2html(this.game.graveyard));
    if (this.activeGuy) {
      $('#input-instructions').html(this.activeGuy.name + ', please play a card with a high value.');
      cards = [];
      cards = this.activeGuy.cards;
      choice = '';
      choice = this.deck2html(cards);
      $('#activeGuyName').html('<h2>Main de ' + this.activeGuy.name + '</h2>');
      $('#input-choice').html(choice);
    }
    tempCount = 0;
    _results = [];
    while (tempCount < this.players.length) {
      $("#player-" + tempCount).html(players[tempCount].status());
      _results.push(tempCount++);
    }
    return _results;
  };

  return DeckDealer;

})();
