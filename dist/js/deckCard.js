
/**
any card
@returns {undefined}
 */
var Card;

Card = (function() {
  Card.prototype.id = null;

  Card.prototype.ownerId = void 0;

  Card.prototype.code = "";

  Card.prototype.name = "";

  Card.prototype.color = "";

  Card.prototype.points = "";

  Card.prototype.css = "card";

  function Card(config) {
    var attrname;
    for (attrname in config) {
      this[attrname] = config[attrname];
    }
    this;
  }

  return Card;

})();
