card-deck
[![Coverage Status](https://coveralls.io/repos/tykayn/card-deck/badge.svg)](https://coveralls.io/r/tykayn/card-deck)
=========

a js tool to simulate card games of all kinds
currently available:
  La Bataille - very simple game of 52 cards.

To run the project

$ npm install -g gulp
$ npm install
$ gulp

