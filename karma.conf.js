// Karma configuration
// Generated on Sun Dec 07 2014 13:13:50 GMT+0100 (CET)
process.env['PHANTOMJS_BIN'] = './node_modules/.bin/phantomjs';

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            // dependencies
            'dist/bower_components/jquery/dist/jquery.min.js',
            'dist/bower_components/angular/angular.min.js',
            'dist/bower_components/angular-mocks/angular-mocks.js',
            // files to test

            'dist/js/deckPlayer.js',
            'dist/js/deckDealer.js',
            'dist/js/deckCard.js',
            'dist/js/deckGame.js',
            'dist/js/deckOfCards.js',
            'dist/js/jeux/bataille.js',
            'dist/js/play.js',
            'dist/js/jeux/deckApp.js',
//      'dist/js/play.js',
            // test cases to run
            'src/tests/tests.js'
        ],


        // list of files to exclude
        exclude: ['dist/bower_components','dist/js/play.js'],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            // source files, that you wanna generate coverage for
            // do not include tests or libraries
            // (these files will be instrumented by Istanbul)
            'dist/js/**/*.js': ['coverage']
        },

        // optionally, configure the reporter
        coverageReporter: {
            type : 'html',
            dir : 'coverage/'
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'coverage'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true
    });
};
