class DeckGame
  name: 'My deck game'
  author: "nobody"
  author_url: "http://github.com/tykayn"
  game_url: "http://github.com/tykayn/card-deck"
  version: "0.1"
  shortDescription: 'my short game description'
  time: '5' #minutes estimées
  oneCard: Card
  rules: 'a deck game' # règles du jeu
  type: 'type'
  nbCards: 52 # nombre de cartes
  nbTurns: 150 # nombre de tours max
  ###
    collections
  ###
  cards: []
  originalDeck: []
  graveyard: []
  other_objects: {
    dices: false # utilisation de dés
#    example de dé
#      [
#        {
#          number: 1
#          values: [1..6]
#          type: 'normal'
#        }
#        {
#          number: 1
#          values: ['give_dice','give_dice','give_dice','1_card','1_card','out']
#          type: 'wasabi'
#        }
#      ]
    money: false
    tokens: false # utilisation de marqueurs
  }
  players: {
    default: 2
    min: 2
    max: 4
  }
  computer: true # jouer contre l'ordi
  webSocket: true # jouer à plusieurs personnes depuis plusieurs connexions
  ###
    cheminement du jeu
  ###
  workflow: {
    shuffle: true
    distributeAll: 6
    start_player: 1
    next_turn: [
      put_card_on_table: 1
      if_table_has: [2, 'fight_cards']
      if_hand_equals: [0, 'win']
    ]
  }
  dealerNewCompetences: [
    {
      name: 'mettre_une_baffe'
      action: (joueur)->
        console.log('le dealer met une baffe au joueur' + joueur.name)
    }
  ]
  # fonctions
  constructor: ->
    @buildCards()
    @autoLoad()
  ###
    rendu html du workflow
  ###
  workflowDisplay: ()->
    @workflow

# chargement du jeu
  autoLoad: ->
    console.log("auto chargement du jeu", @name)
    if DeckDealer
#      console.log("DeckDealer repéré")
#      DeckDealer::addGame(@)
#      console.log(@dealerNewCompetences)
      for competence in @dealerNewCompetences
        functName = competence.name
        newFunc = competence.action
#        console.log(functName)
#        console.log(newFunc)
        DeckDealer[functName] = newFunc
#      console.log('DeckDealer nouvelle fonction', DeckDealer['mettre_une_baffe'])
# construction des cartes
  buildCards: ->
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "V", "D", "R"]
    names = ["as", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Valet", "Dame", "Roi"]
    colors = ["coeur", "trèfle", "carreau", "pique"]
    colors_en = ["heart", "club", "diamond", "spade"]
    colors_icons = ["♥","♣", "♦","♠"]
    htmlIcon = ["&hearts;", "&spades;", "&clubs;", "&diams;"]
    points = [14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    count = 0
    i = 0
    while i < colors.length
      color = colors[i]
      colors_icon = colors_icons[i]
      j = 0
      while j < values.length
        config =
          id: count
          color: color
          colors_icon: colors_icon
          htmlIcon: htmlIcon[i]
          code: j + "-" + color.substring(0, 3)
          name: names[j] + " de " + color
          points: points[j]

        count++
        @addCard new Card(config)
        j++
      i++
# tells how the deck is.
  health: ->
    blah = "i am a deck having " + @cards.length + " cards."
    blah
  tellCards: ->
    i = 0
    while i < @cards.length
      blah += "<br/>" + @cards[i].name
      i++
    blah

# interactions avec jQuery
  jQActions: ->
    console.log('XXXXXXXX pas d\'actions avec jquery')
# remove one card
  removeCard: (card) ->
    cardid = 12
    exitedCard = @cards.pop(cardid)
    @graveyard.push exitedCard
    return
#render html view of a set of cards
  cards2html: (cards)->
#    console.log('cartes a rendre en html : ', cards)
    if not cards
      return ''
    html = ''
    for c in cards
      guyId = parseInt(@playerActive)
      html += '<button class="' + @oneCard.css + ' card card-' + c.color + ' score-'
      html += c.points + ' col-lg-1" data-id="' + c.id + '" data-playerid="'
      html += guyId + '">' + c.name + '</button>'
    html
# add one card
  addCard: (card) ->
    @cards.push card
    @originalDeck.push card
    return
  briefing: ->
#    console.log("briefing du jeu", @name, @shortDescription)
    @shortDescription
