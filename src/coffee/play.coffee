#  init the game components
autotest = 0
loadedGame = new LaBataille()
#loadedGame.briefing()
leDeck  = loadedGame.cards
#console.log "deckOfCards is ready!", leDeck
$('#state').html('sparti')

players = [
  new DeckPlayer(
    id: 0
    name: "bob"
    type: "true-player"
  ),
  new DeckPlayer(
    id: 1
    name: "abrasiveGuy"
    type: "true-player"
  ),
#  new DeckPlayer(
#      id: 2
#      name: "chewbacca"
#      type: "true-player"
#    ),
#  new DeckPlayer(
#      id: 3
#      name: "chuck norris"
#      type: "true-player"
#    )
]
config ={
  players : players
  deck : leDeck
  game : loadedGame
}
dealer = new DeckDealer(config)
dealer.shuffle()
dealer.distributeAll( players, 6)
dealer.jQActions(dealer)
dealer.play()

# auto testing
autoClick = ()->
  console.log "autoclick"
  $('#input-choice button')[0].click()
#  setTimeout autoClick(), 50
#  setTimeout autoClick(), 1000


autoClick() if autotest
