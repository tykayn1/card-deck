
loadedGame = new LaBataille();
players = [new DeckPlayer({
  id  : 0,
  name: "bob",
  type: "true-player"
}),
  new DeckPlayer({
    id  : 1,
    name: "abrasiveGuy",
    type: "true-player"
  })];
config = {
  players: players,
  game   : loadedGame
};
dealer = new DeckDealer(config);
d = dealer;
PlayerBob = d.players[0];
PlayerAbrasiveGuy = d.players[1];
leDeck = d.deck;
console.log('leDeck', leDeck.length);
d.activeGuy = d.players[d.playerToStart];


describe("Deck of cards with 2 players: ", function () {

  beforeEach(function () {
  });

  it("the deck must have a dealer", function () {
    expect(d).toBeDefined();
  });
  it("the deck must have cards", function () {
    expect(d.deck).toBeDefined();
  });

  it("the deck must have 52 cards", function () {
    expect(d.deck.length).toBe(52);
  });
  it("a shuffle must keep the number of cards", function () {
    var length = d.deck.length;
    d.shuffle();
    expect(d.deck.length).toBe(length);
  });
  describe("testing jquery : ", function () {
    it("there must be jquery defined", function () {
      expect(jQuery).toBeDefined();
    });
    it("there must be jquery actions defined", function () {
      expect(d.jQActions).toBeDefined();

      var liste = d.jQActions(d);
      expect(liste).toBeTruthy();
    });
  });
  describe("testing the cards dealer : ", function () {
    it("there must be the dealer defined", function () {
      expect(d).toBeDefined();
    });
    it("must set the current game la Bataille", function () {
      expect(d.setGame).toBeDefined();
      d.setGame(loadedGame);
      //expect(d.jeux.length).toBe(1);
      expect(d.game).toBe(loadedGame);
    });
  });


  describe("testing players : ", function () {
    it("there must be at least one player", function () {
      expect(d.players.length).toBeGreaterThan(1);
    });
    it("the first player must have no cards at the beginning", function () {
      expect(d.players[0].cards.length).toBe(0);
    });

    it("the first player must have a score of 0 points at the beginning", function () {
      expect(d.players[0].score).toBe(0);
    });

    it("must give 10 cards to the first player", function () {
      d.distribute(PlayerBob, 10);
      expect(PlayerBob.cards.length).toBe(10);
      expect(d.deck.length).toBe(52 - 10);
    });
    it("must reinit the deck to 52", function () {
      expect(d.players.length).toBe(2);
    });
    it("must drop playerBob's cards", function () {
      PlayerBob.dropHand();
      expect(PlayerBob.cards.length).toBe(0);
    });
    it("must drop all players cards", function () {
      d.killAllHands();
      expect(PlayerBob.cards.length).toBe(0);
      expect(PlayerAbrasiveGuy.cards.length).toBe(0);
    });
    it("there must be 2 players", function () {
      expect(d.players.length).toBe(2);
    });
    it("distributes 6 cards to the 2 players", function () {
      // réinit du paquet de cartes
      d.deck = loadedGame.originalDeck;
      expect(d.deck.length).toBe(52);
      var nbCards = 6;
      d.distributeAll(d.players, nbCards);
      expect(d.deck.length).toBe(52 - (nbCards * 2));
      expect(PlayerBob.cards.length).toBe(nbCards);
      expect(PlayerAbrasiveGuy.cards.length).toBe(nbCards);
    });

  });
  describe("testing the game la Bataille : ", function () {
    it("there must be la Bataille defined", function () {
      expect(loadedGame).toBeDefined();
      //expect(d.jeux.length).toBeTruthy();
    });
    it("must convert cards to html", function () {
      expect(loadedGame.cards2html).toBeDefined();
      var cards = d.players[0].cards;
      var html = loadedGame.cards2html(cards);
      expect(html.length).toBeTruthy();
    });
    it("must convert no cards to no html", function () {
      expect(loadedGame.cards2html).toBeDefined();
      var cards = [];
      var html = loadedGame.cards2html(cards);
      expect(html.length).toBeFalsy();
    });
  });
  describe("testing table and first player : ", function () {
    it("the table must have 0 cards at the beginning", function () {
      // réinit du paquet de cartes
      d.deck = loadedGame.originalDeck;
      expect(d.table.length).toBe(0);
    });
    xit("the first player gives 1 card to the table, it must have one card now", function () {
      d.putCardToTable(PlayerBob.cards[0]);
      expect(d.table.length).toBe(1);
    });
    //it("the first player must have 9 cards after putting one on the table", function () {
    //    expect(PlayerBob.cards.length).toBe(9);
    //});
  });

});